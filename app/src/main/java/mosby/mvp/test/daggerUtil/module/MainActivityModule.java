package mosby.mvp.test.daggerUtil.module;


import dagger.Module;
import dagger.Provides;
import mosby.mvp.test.MainActivity;
import mosby.mvp.test.daggerUtil.scopes.MainActivityScope;

/**
 * Created by Taher on 22/04/2017.
 * Project: DaggerTutorial
 */

@Module
public class MainActivityModule {
    private final MainActivity mainActivity;

    public MainActivityModule(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Provides
    @MainActivityScope
    public MainActivity mainActivity(){
        return mainActivity;
    }

}
