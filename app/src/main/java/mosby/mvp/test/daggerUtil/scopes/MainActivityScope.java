package mosby.mvp.test.daggerUtil.scopes;

import javax.inject.Scope;

/**
 * Created by Taher on 22/04/2017.
 * Project: DaggerTutorial
 */

@Scope
public @interface MainActivityScope {
}
