package mosby.mvp.test.daggerUtil.module;


import dagger.Module;
import dagger.Provides;
import mosby.mvp.test.api.ApiService;
import mosby.mvp.test.daggerUtil.qualifier.ApiServiceQualifier;
import mosby.mvp.test.daggerUtil.qualifier.RetrofitQualifier;
import mosby.mvp.test.daggerUtil.scopes.DaggerTutorialApplicationScope;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = NetworkModule.class)
public class ApiServiceModule {
    private static final String BASE_URL = "http://api.icndb.com/";

    @Provides
    @ApiServiceQualifier
    @DaggerTutorialApplicationScope
    public ApiService apiService(@RetrofitQualifier Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    @Provides
    @RetrofitQualifier
    @DaggerTutorialApplicationScope
    public Retrofit retrofit(OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }


    @Provides
    @DaggerTutorialApplicationScope
    public ApiService anotherApiService(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    @Provides
    @DaggerTutorialApplicationScope
    public Retrofit anotherRetrofit(OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }
}
