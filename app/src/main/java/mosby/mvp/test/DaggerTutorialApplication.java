package mosby.mvp.test;

import android.app.Activity;
import android.app.Application;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.io.File;

import mosby.mvp.test.api.ApiService;
import mosby.mvp.test.daggerUtil.component.DaggerDaggerTutorialApplicationComponent;
import mosby.mvp.test.daggerUtil.component.DaggerTutorialApplicationComponent;
import mosby.mvp.test.daggerUtil.module.ApiServiceModule;
import mosby.mvp.test.daggerUtil.module.ContextModule;
import mosby.mvp.test.daggerUtil.module.NetworkModule;
import mosby.mvp.test.daggerUtil.module.PicassoModule;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

public class DaggerTutorialApplication extends Application {
    DaggerTutorialApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
        component = DaggerDaggerTutorialApplicationComponent.builder()
                .contextModule(new ContextModule(this))
                .build();


    }

    public DaggerTutorialApplicationComponent component() {
        return component;
    }

    public static DaggerTutorialApplication get(Activity activity) {
        return (DaggerTutorialApplication) activity.getApplication();
    }

}

