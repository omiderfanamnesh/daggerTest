package mosby.mvp.test.daggerUtil.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import mosby.mvp.test.model.Contact;
import mosby.mvp.test.model.User;

@Module
public class UserModule {

    @Provides
    @Singleton
    Contact provideContact() {
        return new Contact();
    }

    @Provides
    @Singleton
    User provideUser() {
        return new User(new Contact());
    }
}