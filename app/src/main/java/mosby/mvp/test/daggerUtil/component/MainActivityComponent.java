package mosby.mvp.test.daggerUtil.component;


import dagger.Component;

import mosby.mvp.test.MainActivity;
import mosby.mvp.test.daggerUtil.module.MainActivityModule;
import mosby.mvp.test.daggerUtil.scopes.MainActivityScope;

/**
 * Created by Taher on 22/04/2017.
 * Project: DaggerTutorial
 */

@Component(modules = MainActivityModule.class, dependencies = DaggerTutorialApplicationComponent.class)
@MainActivityScope
public interface MainActivityComponent {
    void injectMainActivity(MainActivity mainActivity);
}
